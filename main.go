package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	podLabelSelector = flag.String("pod-label-selector", "name!=tiller,app.kubernetes.io/name!=postgresql,app!=redis", "Label selector for deployments")
	namespaceFilter  = flag.String("namespace-filter", "app", "Filter for namespaces")
	gtcpu            = flag.Int64("cpu", 0, "Prints deployment only if requested resources CPU greater than specified")
	gtmem            = flag.Float64("mem", 0, "Prints deployment only if requested resources Memory greater than specified")
)

func realMain() error {
	flag.Parse()

	client, err := NewClientSet()
	if err != nil {
		return err
	}

	namespaceList, err := client.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	namespaceFilterRE := regexp.MustCompile(*namespaceFilter)
	for _, namespace := range namespaceList.Items {
		if !namespaceFilterRE.MatchString(namespace.Name) {
			continue
		}
		if err := processNamespace(client, namespace.Name); err != nil {
			return err
		}
	}

	return nil
}

func processNamespace(client kubernetes.Interface, namespace string) error {
	listOpts := metav1.ListOptions{
		LabelSelector: *podLabelSelector,
	}
	podList, err := client.CoreV1().Pods(namespace).List(context.Background(), listOpts)
	if err != nil {
		return err
	}

	var items []string
	for _, pod := range podList.Items {
		if pod.Status.Phase != corev1.PodRunning {
			continue
		}

		var (
			cpuRequests    int64
			cpuLimits      int64
			memoryRequests float64
			memoryLimits   float64
			containers     []string
		)
		for _, container := range pod.Spec.Containers {
			containers = append(containers, container.Name)
			if cpu := container.Resources.Requests.Cpu(); cpu != nil {
				cpuRequests += cpu.MilliValue()
			}
			if cpu := container.Resources.Limits.Cpu(); cpu != nil {
				cpuLimits += cpu.MilliValue()
			}
			if mem := container.Resources.Requests.Memory(); mem != nil {
				memoryRequests += float64(mem.Value()) / 1024 / 1024
			}
			if mem := container.Resources.Limits.Memory(); mem != nil {
				memoryLimits += float64(mem.Value()) / 1024 / 1024
			}
		}

		if cpuRequests <= *gtcpu || memoryRequests <= *gtmem {
			continue
		}

		sort.Strings(containers)
		line := fmt.Sprintf(
			"   %s (%s): cpu: %dm (%dm), mem: %.0fM (%.0fM)",
			pod.Name,
			strings.Join(containers, "+"),
			cpuRequests,
			cpuLimits,
			memoryRequests,
			memoryLimits,
		)
		items = append(items, line)
	}

	if len(items) > 0 {
		fmt.Printf("=== %s\n", namespace)
		fmt.Println(strings.Join(items, "\n"))
	}

	return nil
}

func main() {
	if err := realMain(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
