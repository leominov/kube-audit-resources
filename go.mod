module gitlab.qleanlabs.ru/platform/infra/kube-audit-resources

go 1.16

require (
	k8s.io/api v0.22.1
	k8s.io/apimachinery v0.22.1
	k8s.io/client-go v0.22.1
)
