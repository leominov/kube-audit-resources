# kube-audit-resources

## Usage

```
Usage of ./kube-audit-resources:
  -cpu int
    	Prints deployment only if requested resources CPU greater than specified
  -deployment-label-selector string
    	Label selector for deployments (default "name!=tiller")
  -mem float
    	Prints deployment only if requested resources Memory greater than specified
  -namespace-filter string
    	Filter for namespaces (default "app")
```
